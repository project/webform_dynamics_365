<?php

namespace Drupal\webform_dynamics_365\Controller;

use Drupal\Component\Utility\Html;
Use GuzzleHttp\Exception\RequestException;

/**
 * Controller routines for Webform Dynamics 365 pages.
 */
class WebformDynamics365Controller {

  /**
   * Constructs case lookup result with arguments.
   * This callback is mapped to the path
   * '/i-want/book-report-and-inquire/serviceking-customer-centre/case-lookup/{paragraphs}/{phrases}'.
   * 
   * TODO: Somehow add a hook to create an alias using pathauto
   * 
   * @param string $case_number
   *   The case number of the incident
   * @param string $full_name
   *   The full name of the resident
   */
  public function case_lookup($case_number, $full_name) {
    // Get settings.
    $config = \Drupal::config('webform_dynamics_365.settings');

    $url = $config->get('url');
    $api_version = $config->get('api_version');
    $username = $config->get('username');
    $password = $config->get('password');
    $test = $config->get('test');

    $request_url = $url.'/api/data/v'.$api_version;

    // Page elements
    $page['#title'] = Html::escape('Case Lookup Results');

    // Add theme.
    $page['#theme'] = 'case_lookup_not_found';

    if (empty($url) || empty($api_version) || empty($username) || empty($password)) {
      drupal_set_message(t('Webform Dynamics 365 is not configured.'), 'error');
    }

    $incident = $contact = NULL;
    $render = FALSE;

    if(empty($test)) {
      try {
        $client = \Drupal::httpClient();

        // Get incident using case number
        $incidents_url = $request_url.'/incidents?$filter=contains(ticketnumber,\''.Html::escape($case_number).'\')';
        $request = $client->get($incidents_url, ['auth' => [$username, $password, 'ntlm']]);
        $incident = json_decode($request->getBody());

        // Get contact using full name or email
        $contacts_url = $request_url.'/contacts?$filter=fullname eq \''.Html::escape($full_name).'\' or emailaddress1 eq \''.Html::escape($full_name).'\'';
        $request = $client->get($contacts_url, ['auth' => [$username, $password, 'ntlm']]);
        $contact = json_decode($request->getBody());

        if(empty($incident->value) || empty($contact->value)) {
          if(empty($incident->value)) {
            drupal_set_message(t('Unable to find case number.'), 'error');
          }
    
          if(empty($contact->value)) {
            drupal_set_message(t('Unable to find resident.'), 'error');
          }
        } else {
          // Get list of contact IDs
          $contact_ids = [];

          foreach($contact->value as $contact_data) {
            $contact_ids[] = $contact_data->contactid;
          }

          if(in_array($incident->value[0]->_customerid_value, $contact_ids)) {
            $render = TRUE;
          } else {
            drupal_set_message(t('Unable to retrieve case.'), 'error');
          }
        }
      } catch (RequestException $request_exception) {
        // Encode HTML entities to prevent broken markup from breaking the page.
        $message = $request_exception->getMessage();
        drupal_set_message(t($message), 'error');
      }
    }
    
    if($render || !empty($test)) {
      $statuscodes = array(
        1 => 'In Progress',
        2 => 'On Hold',
        3 => 'Waiting for Details',
        4 => 'Resarching',
        5 => 'Resolved',
        100000000 => 'Open', // New
        100000001 => 'Assigned'
      );

      $wards = array(
        100000000 => 'Township Wide',
        100000001 => 'Ward 1',
        100000002 => 'Ward 2',
        100000003 => 'Ward 3',
        100000004 => 'Ward 4',
        100000005 => 'Ward 5',
        100000006 => 'Ward 6'
      );

      if(!empty($test)) {
        $incident = (object) array(
          'value' => array(
            array(
              'ticketnumber' => $case_number,
              'createdon' => date(time()),
              'statusdescription' => 'Open',
              'title' => 'Test Title',
              'warddescription' => 'Township Wide'
            )
          )
        );

        $contact = (object) array(
          'value' => array(
            array(
              'fullname' => $full_name,
            )
          )
        );
      } else {
        // Add status and ward descriptions
        $incident->value[0]->statusdescription = $statuscodes[$incident->value[0]->statuscode];
        $incident->value[0]->warddescription = $wards[$incident->value[0]->new_ward];
      }

      $page['#incident'] = $incident->value[0];
      $page['#contact'] = $contact->value[0];
      
      // Add theme.
      $page['#theme'] = 'case_lookup_results';
    }

    return $page;
  }
}