<?php

namespace Drupal\webform_dynamics_365\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use GuzzleHttp\Exception\RequestException;
use SaintSystems\OData\ODataClient;
use SaintSystems\OData\GuzzleHttpProvider;
Use Doctrine\Common\Inflector\Inflector;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Create a new case from a webform submission.
 *
 * @WebformHandler(
 *   id = "Create an entity",
 *   label = @Translation("Create an incident"),
 *   category = @Translation("Webform Dynamics 365"),
 *   description = @Translation("Creates a new incident in Webform Dynamics 365 from webform submission."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class WebformDynamics365CreateIncidentWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $field_names = array_keys(\Drupal::service('entity_field.manager')->getBaseFieldDefinitions('webform_submission'));
    $excluded_data = array_combine($field_names, $field_names);
    return [
      'contact_entity_name' => '',
      'incident_entity_name' => '',
      'incident_entity_contact_source' => '',
      'incident_entity_contact_destination' => '',
      'excluded_data' => $excluded_data,
      'debug' => FALSE,
      'contact_field_mapping' => [],
      'incident_field_mapping' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webform = $this->getWebform();
    $elements = $webform->getElementsInitializedFlattenedAndHasValue('view');
    $fields = [];

    if(!empty($elements)) {
      foreach($elements as $key => $element) {
        $fields[$key] = $element['#admin_title'] ?: $element['#title'] ?: $key;

        if($fields[$key] != $key) {
          $fields[$key] = $fields[$key].' ('.$key.')';
        }
      }
    }

    // Entity
    $form['entity'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entities'),
    ];
    $form['entity']['contact_entity_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['contact_entity_name'],
    ];
    $form['entity']['incident_entity_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Incident'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['incident_entity_name'],
    ];
    $form['entity']['incident_entity_contact_source'] = [
      '#type' => 'select',
      '#title' => $this->t('Contact Source'),
      '#options' => $fields,
      '#default_value' => $this->configuration['incident_entity_contact_source'],
      '#required' => TRUE,
    ];
    $form['entity']['incident_entity_contact_destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Contact Destination'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['incident_entity_contact_destination'],
    ];

    // Contact Mapping
    $form['contacts'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact Field Mapping'),
    ];

    $form['contacts']['contact_field_mapping'] = [
      '#type' => 'webform_multiple',
      '#empty_items' => 0,
      '#no_items_message' => $this->t('Please add fields below to map to contact entity.'),
      '#add' => FALSE,
      '#element' => [
        'source' => [
          '#type' => 'webform_select_other',
          '#title' => $this->t('Source'),
          '#options' => $fields,
        ],
        'destination' => [
          '#type' => 'textfield',
          '#title' => $this->t('Destination'),
          '#required' => TRUE,
        ],
        'lookup' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Use for lookup')
        ],
      ],
      '#default_value' => $this->configuration['contact_field_mapping'],
    ];

    // Incident Mapping
    $form['incidents'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Incident Field Mapping'),
    ];

    $form['incidents']['incident_field_mapping'] = [
      '#type' => 'webform_multiple',
      '#empty_items' => 0,
      '#no_items_message' => $this->t('Please add fields below to map to incident entity.'),
      '#add' => FALSE,
      '#element' => [
        'source' => [
          '#type' => 'webform_select_other',
          '#title' => $this->t('Source'),
          '#options' => $fields,
        ],
        'destination' => [
          '#type' => 'textfield',
          '#title' => $this->t('Destination'),
          '#required' => TRUE,
        ],
        'store' => [
          '#type' => 'checkbox',
          '#title' => $this->t('Store destination to source')
        ],
      ],
      '#default_value' => $this->configuration['incident_field_mapping'],
    ];

    // Submission data.
    $form['submission_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Submission data'),
    ];
    // Display warning about file uploads.
    if ($webform->hasManagedFile()) {
      $form['submission_data']['managed_file_message'] = [
        '#type' => 'webform_message',
        '#message_message' => $this->t('Upload files will include the file\'s id, name, uri, and data (<a href=":href">Base64</a> encode).', [':href' => 'https://en.wikipedia.org/wiki/Base64']),
        '#message_type' => 'warning',
        '#message_close' => TRUE,
        '#message_id' => 'webform_node.references',
        '#message_storage' => WebformMessage::STORAGE_SESSION,
      ];
    }
    $form['submission_data']['excluded_data'] = [
      '#type' => 'webform_excluded_columns',
      '#title' => $this->t('Posted data'),
      '#title_display' => 'invisible',
      '#webform_id' => $webform->id(),
      '#required' => TRUE,
      '#default_value' => $this->configuration['excluded_data'],
    ];

    $this->elementTokenValidate($form);

    // Development.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development Settings'),
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];

    return  $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
    if ($this->configuration['method'] === 'GET') {
      $this->configuration['type'] = '';
    }
  }

  /**
   * {@inheritdoc}
   */

  // Function to be fired after submitting the Webform.
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    //TODO: I need a class that handles the authentication from config
    if(!$update) { // New created submission
      $config = \Drupal::config('webform_dynamics_365.settings');
      
      // Get an array of the values from the submission.
      $sid = $webform_submission->id();
      $webform_data = $webform_submission->getData();

      // Get field mapping
      $contact_field_mapping = [];
      $incident_field_mapping = [];

      if(!empty($this->configuration['contact_field_mapping'])) {
        foreach($this->configuration['contact_field_mapping'] as $field_map) {
          if(!empty($field_map['source']) && !empty($field_map['destination'])) {
            $contact_field_mapping[$field_map['source']] = $field_map['destination'];
          }
        }
      }

      if(!empty($this->configuration['incident_field_mapping'])) {
        foreach($this->configuration['incident_field_mapping'] as $field_map) {
          if(!empty($field_map['source']) && !empty($field_map['destination']) && empty($field_map['store'])) {
            $incident_field_mapping[$field_map['source']] = $field_map['destination'];
          }
        }
      }
      
      if(empty($contact_field_mapping) && empty($incident_field_mapping)) {
        // Display no mappped fields warning
        $context = [
          '@message' => $this->t('There are no mapped fields.'),
          '@form' => $this->getWebform()->label(),
        ];

        $this->getLogger('webform_dynamics_365')
          ->warning('@form - @message', $context);
        return;
      }

      // Map the fields
      $contact_data = [];
      $incident_data = [];

      if(!empty($webform_data)) {
        $contact_field_mapping_keys = array_keys($contact_field_mapping);

        foreach($webform_data as $key => $value) {
          if(in_array($key, $contact_field_mapping_keys)) {
            $field = $contact_field_mapping[$key];

            if(preg_match('/(.+id\_(.+)|(.+)id)@odata\.bind$/', $field, $matches)) {
              $entity = $matches[2] ?: $matches[3];
              $entity_set = Inflector::pluralize($entity);

              if(!empty($value)) {
                $contact_data[$field] = $entity_set.'('.$value.')';
              }
            } else {
              if(!empty($value)) {
                $contact_data[$field] = $value;
              }
            }
          }

          $incident_field_mapping_keys = array_keys($incident_field_mapping);
          
          if(in_array($key, $incident_field_mapping_keys)) {
            $field = $incident_field_mapping[$key];

            if(preg_match('/(.+id\_(.+)|(.+)id)@odata\.bind$/', $field, $matches)) {
              $entity = $matches[2] ?: $matches[3];
              $entity_set = Inflector::pluralize($entity);

              if(!empty($value)) {
                $incident_data[$field] = $entity_set.'('.$value.')';
              }
            } else {
              if(!empty($value)) {
                $incident_data[$field] = $value;
              }
            }
          }
        }
      }

      try {
        // Create httpProvider from GuzzleHttpProvider with ntlm authentication
        //TODO: We might need to diffirentiate between ntlm or basic auth
        // See: https://github.com/saintsystems/odata-client-php/wiki/Example-Calls
        // https://docs.microsoft.com/en-us/powerapps/developer/common-data-service/webapi/query-data-web-api
        //TODO: Put this is a new class that returns a Dyanamic365 instance
        //SEE: smtp module
        //TODO:: If username contains \ use ntlm, otherwise basic?


        $url = $config->get('url');
        $api_version = $config->get('api_version');
        $username = $config->get('username');
        $password = $config->get('password');

        $request_url = $url.'/api/data/v'.$api_version;
        $request_method = 'post';
        $contact_entity_name = $this->configuration['contact_entity_name'];
        $contact_entity_set = Inflector::pluralize($contact_entity_name);
        $incident_entity_name = $this->configuration['incident_entity_name'];
        $incident_entity_set = Inflector::pluralize($incident_entity_name);
        
        $http_provider = new GuzzleHttpProvider();
        $http_provider->setExtraOptions(['auth' => [$username, $password, 'ntlm']]);
        
        $odata_client = new ODataClient($request_url, null, $http_provider);

        if(!empty($this->configuration['contact_field_mapping'])) {
          $contact = null;
          
          // Find contact using lookup fields
          foreach($this->configuration['contact_field_mapping'] as $contact_field_map) {
            if(!empty($contact_field_map['lookup'])) {
              $value = $contact_data[$contact_field_map['destination']];
              
              if(!empty($value)) {
                $contacts = $odata_client->from($contact_entity_set)->where($contact_field_map['destination'], '=', $value)->order('createdon', 'desc')->get();


                if(!empty($contacts) && $contacts->count() > 0) {
                  $contact = $contacts->first();

                  //TODO: Add option to update contact info from webform

                  $context = [
                    '@entity' => $contact_entity_name,
                  ];
            
                  $this->getLogger('webform_dynamics_365')
                    ->notice('New @entity created.', $context);
                  break;
                } else {
                  $contact = null;
                }
              }

              if(empty($contact)) {
                echo 'Contact Not Found';
              } else {
                echo 'Contact Found';
              }
            }
          }

          if(empty($contact)) {
            // Create a contact
            $data = json_encode($contact_data);
            $result = $odata_client->post($contact_entity_set, $data);

            if(!empty($result) && count($result) > 0) {
              if(empty($contact)) {
                echo 'Contact Created';
              }
              
              // Find the created contact
              foreach($this->configuration['contact_field_mapping'] as $contact_field_map) {
                if(!empty($contact_field_map['lookup'])) {
                  $value = $contact_data[$contact_field_map['destination']];
                  
                  if(!empty($value)) {
                    $contacts = $odata_client->from($contact_entity_set)->where($contact_field_map['destination'], '=', $value)->order('createdon', 'desc')->get();
      
                    if(!empty($contacts) && $contacts->count() > 0) {
                      $contact = $contacts->first();

                      $context = [
                        '@entity' => $contact_entity_name,
                      ];
                
                      $this->getLogger('webform_dynamics_365')
                        ->notice('New @entity created.', $context);
                      break;
                    } else {
                      $contact = null;
                    }
                  }

                  //TODO: Use error handling instead
                  if(empty($contact)) {
                    echo 'Contact Not Found';
                  } else {
                    echo 'Contact Found';
                  }
                }
              }
            }
          }
        }

        if(!empty($contact)) {
          if(!empty($this->configuration['incident_entity_contact_source']) && !empty($this->configuration['incident_entity_contact_destination'])) {
            // Bind contact to incident
            $incident_data[$this->configuration['incident_entity_contact_destination']] = $contact_entity_set.'('.$contact->contactid.')';

            // Store resident id
            $webform_data[$this->configuration['incident_entity_contact_destination']] = $contact->contactid;

            // Set submission data.
            $webform_submission->setData($webform_data);

            // Save submission (do not trigger hooks)
            $webform_submission->resave();
          }
            
          // Create incident
          $data = json_encode($incident_data);
          $result = $odata_client->post($incident_entity_set, $data);

          if(!empty($result) && count($result) > 0) {
            $incidents = $odata_client->from($incident_entity_set)->where('title', '=', $incident_data['title'])->order('createdon', 'desc')->get();

            if(!empty($contacts) && $contacts->count() > 0) {
              $incident = $incidents->first();

              // Check if there are any fields to be stored from the destination to the source
              if(!empty($this->configuration['incident_field_mapping'])) {
                $resave = FALSE;

                foreach($this->configuration['incident_field_mapping'] as $field_map) {
                  if(!empty($field_map['source']) && !empty($field_map['destination']) && !empty($field_map['store'])) {
                    if(!empty($incident->{$field_map['destination']})) {
                      $webform_data[$field_map['source']] = $incident->{$field_map['destination']};
                      $resave = TRUE;
                    }
                  }
                }

                if($resave) {
                  // Set submission data.
                  $webform_submission->setData($webform_data);

                  // Save submission (do not trigger hooks)
                  $webform_submission->resave();
                }
              }

              $context = [
                '@entity' => $incident_entity_name,
              ];
        
              $this->getLogger('webform_dynamics_365')
                ->notice('New @entity created.', $context);
            }
          }
        }
      } catch (RequestException $request_exception) {
        $response = $request_exception->getResponse();

        // Encode HTML entities to prevent broken markup from breaking the page.
        $message = $request_exception->getMessage();
        $message = nl2br(htmlentities($message));

        $this->handleError($message, $request_url, $request_method, $response);
      }
    }
  }

  /**
   * Handle error by logging and display debugging and/or exception message.
   *
   * @param string $message
   *   Message to be displayed.
   * @param string $request_url
   *   The remote URL the request is being posted to.
   * @param string $request_method
   *   The method of remote post.
   * @param string $request_type
   *   The type of remote post.
   * @param \Psr\Http\Message\ResponseInterface|null $response
   *   The response returned by the remote server.
   */
  protected function handleError($message, $request_url, $request_method, $response) {
    // Log error message.
    $context = [
      '@form' => $this->getWebform()->label(),
      '@method' => $request_method,
      '@url' => $request_url,
      '@message' => $message,
      'webform_submission' => $this->getWebformSubmission(),
      'handler_id' => $this->getHandlerId(),
      'operation' => 'error',
      'link' => $this->getWebform()
        ->toLink($this->t('Edit'), 'handlers')
        ->toString(),
    ];

    $this->getLogger('webform_dynamics_365')
      ->error('@form @method to @url failed. @message', $context);

    $type = $response ? MessengerInterface::TYPE_ERROR : MessengerInterface::TYPE_STATUS;
    $this->messenger()->addMessage($message, $type);
  }
}