<?php

namespace Drupal\webform_dynamics_365\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'Case Lookup' Block.
 *
 * @Block(
 *   id = "case_lookup_block",
 *   admin_label = @Translation("Case Lookup"),
 *   category = @Translation("Webform Dynamics 365"),
 * )
 */
class CaseLookupBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Return the form @ Form/CaseLookupBlockForm.php.
    return \Drupal::formBuilder()->getForm('Drupal\webform_dynamics_365\Form\CaseLookupBlockForm');
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'webform_dynamics_365 case lookup');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('loremipsum_block_settings', $form_state->getValue('loremipsum_block_settings'));
  }

}