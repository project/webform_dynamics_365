<?php

namespace Drupal\webform_dynamics_365\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Exception\RequestException;
use SaintSystems\OData\ODataClient;
use SaintSystems\OData\GuzzleHttpProvider;

/**
 * Configure Cyberimpact settings for this site.
 */
class WebformDynamics365AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_dynamics_365_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_dynamics_365.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings
    $config = $this->config('webform_dynamics_365.settings');

    $password = $config->get('password');

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('url'),
      '#required' => true
    ];
    $form['api_version'] = [
      '#type' => 'select',
      '#title' => $this->t('API version'),
      '#options' => [
        '8.2' => $this->t('8.2')
      ],
      '#required' => true,
      '#default_value' => $config->get('api_version')
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('username'),
      '#required' => true
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#default_value' => $password,
      '#description' => !empty($password) ? $this->t('Password set. Enter new password.') : null,
      '#required' => empty($password),
    ];
    $form['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test mode'),
      '#default_value' => $config->get('test'),
      '#description' => $this->t('Enable to skip connecting to the source and display test data.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webform_dynamics_365.settings');

    // New config
    $url = $form_state->getValue('url');
    $api_version = $form_state->getValue('api_version');
    $username = $form_state->getValue('username');
    $password = $form_state->getValue('password');
    $test = $form_state->getValue('test');

    if(empty($password)) {
      // Use saved password
      $password = $config->get('password');
    }

    if(empty($test)) {
      $request_url = $url.'/api/data/v'.$api_version;

      //TODO:: If username contains \ use ntlm, otherwise basic?
      $http_provider = new GuzzleHttpProvider();
      $http_provider->setExtraOptions(['auth' => [$username, $password, 'ntlm']]);

      $odataClient = new ODataClient($request_url, null, $http_provider);

      try {
        // Try to connect
        $odataClient->from('accounts')->get();
      } catch (RequestException $request_exception) {
        $message = $request_exception->getMessage();

        $form_state->setErrorByName('url', $message);
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('webform_dynamics_365.settings');

    $password = $config->get('password');
    $newPassword = $form_state->getValue('password');
    
    $config->set('url', $form_state->getValue('url'));
    $config->set('api_version', $form_state->getValue('api_version'));
    $config->set('username', $form_state->getValue('username'));
    $config->set('test', $form_state->getValue('test'));

    if(empty($password) || !empty($newPassword)) {
      $config->set('password', $newPassword);
    }

    $config->save();

    return parent::submitForm($form, $form_state);
  }

}
