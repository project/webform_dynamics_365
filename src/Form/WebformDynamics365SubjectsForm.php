<?php

namespace Drupal\webform_dynamics_365\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
Use GuzzleHttp\Exception\RequestException;

/**
 * Configure Cyberimpact settings for this site.
 */
class WebformDynamics365SubjectsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_dynamics_365_admin_subjects';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['webform_dynamics_365.subjects'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get settings.
    $config = \Drupal::config('webform_dynamics_365.settings');

    $url = $config->get('url');
    $api_version = $config->get('api_version');
    $username = $config->get('username');
    $password = $config->get('password');

    $request_url = $url.'/api/data/v'.$api_version;

    if (empty($url) || empty($api_version) || empty($username) || empty($password)) {
      drupal_set_message(t('Webform Dynamics 365 is not configured.'), 'error');
    }

    $client = \Drupal::httpClient();

    // Get subjects
    $incidents_url = $request_url.'/subjects';
    $request = $client->get($incidents_url, ['auth' => [$username, $password, 'ntlm']]);
    $subjects = json_decode($request->getBody(), true);
    $data = array();

    // Sort by created time
    usort($subjects['value'], function($a, $b) {
      return strtotime($a['createdon']) <=> strtotime($b['createdon']);
    });

    // Create 2 level tree
    foreach($subjects['value'] as $item) {
      if(empty($item['_parentsubject_value'])) {
        $data[$item['subjectid']] = $item;
      } else {
        if(array_key_exists($item['_parentsubject_value'], $data)) {
          $data[$item['_parentsubject_value']]['children'][$item['subjectid']] = $item;
        } else {
          foreach($data as $key => $value) {
            if(!empty($value['children'])) {
              if(in_array($item['_parentsubject_value'], array_keys($value['children']))) {
                $data[$key]['children'][$item['subjectid']] = $item;
                break;
              }
            } else {
              $data[$key]['children'][$item['subjectid']] = $item;
            }
          }
        }
      }
    }

    // Sort by title
    usort($data, function($a, $b) {
      return $a['title'] <=> $b['title'];
    });

    $html = '<p>'
      .$this->t('List of Subjects with unique IDs extracted from CRM.')
    .'</p>';

    // Render markup
    $html .= '<ul>';

    foreach($data as $subject) {
      $html .= '<li>'
        ."{$subject['subjectid']}: {$subject['title']}";

      if(!empty($subject['children'])) {
        $children = $subject['children'];

        usort($children, function($a, $b) {
          return $a['title'] <=> $b['title'];
        });

        $html .= '<ul>';

        foreach($children as $child) {
          $html .= '<li>'
            ."{$child['subjectid']}: {$child['title']}"
          .'</li>';
        }

        $html .= '</ul>';
      }

      $html .= '</li>';
    }

    $html .= '</ul>';

    return [
      '#markup' => $html
    ];
  }
}