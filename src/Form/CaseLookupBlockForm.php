<?php

namespace Drupal\webform_dynamics_365\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Case Lookup block form
 */
class CaseLookupBlockForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'case_lookup_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('webform_dynamics_365.settings');

    $test = $config->get('test');

    if(!empty($test)) {
      drupal_set_message(t('Webform Dynamics 365 test mode enabled'), 'warning');
    }

    // Case Number
    $form['case_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Case Number'),
      '#placeholder' => 'CRM-#####-######',
      //'#value' => 'CRM-24222-M0T2X7', // Test
      '#description' => $this->t('The case # you were provided with.'),
    ];

    // Full Name
    $form['full_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name or E-mail Address'),
      //'#placeholder' => 'Full Name or E-mail Address',
      //'#value' => 'Paul McCartney', // Test
      '#description' => $this->t('Your full name or e-mail address'),
    ];

    // Submit.
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $case_number = $form_state->getValue('case_number');
    $full_name = $form_state->getValue('full_name');

    if(empty($case_number) || !preg_match('/CRM\-[\d]{5}\-[\w]{6}/', $case_number)) {
      $form_state->setErrorByName('case_number', $this->t('Invalid case number format.'));
    }

    if(empty($full_name)) {
      $form_state->setErrorByName('full_name', $this->t('Enter full name.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('webform_dynamics_365.case_lookup', [
      'case_number' => $form_state->getValue('case_number'),
      'full_name' => $form_state->getValue('full_name')
    ]);
  }
}